This is code modified from the Queen City Regional FRC 2016, for use with the 2017 robot.
CTRE library now contains the CANTalon library.

It uses command based programming, WPI robot builder, with modifications. It is set for tank drive, using two Joysticks. It uses buttons on the joysticks to utilize the commands, including, actuating a double solenoid, and running a PID motor forwards and backwards.